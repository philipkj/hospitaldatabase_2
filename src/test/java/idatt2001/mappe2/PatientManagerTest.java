package idatt2001.mappe2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for the methods in PatientManager.java
 * @version 05.05.2021
 * @author philipkj
 */
public class PatientManagerTest {

    public static PatientManager testPatientManager;

    /**
     * Attempts to add valid patient
     */
    @Test
    void testAddPatientTrue(){
        testPatientManager = new PatientManager();
        Patient p1 = new Patient("Michael","Jackson",1111);
        assertTrue(testPatientManager.addPatient(p1));
    }

    /**
     * Attempts to add patient with duplicate socialSecurityNumber
     */
    @Test
    void testAddPatientFalse(){
        testPatientManager = new PatientManager();
        Patient p2 = new Patient("Erykah","Badu",2222);
        testPatientManager.addPatient(p2);
        Patient p3 = new Patient("Alicia","Keys",2222);
        assertFalse(testPatientManager.addPatient(p3));
    }

    /**
     * Attempts to remove patient existing in list
     */
    @Test
    void testRemovePatientTrue(){
        testPatientManager = new PatientManager();
        Patient p4 = new Patient("David","Bowie",4444);
        testPatientManager.addPatient(p4);
        assertTrue(testPatientManager.removePatient(p4.getSocialSecurityNumber()));
    }

    /**
     * Attempts to remove patient not existing in the list
     */
    @Test
    void testRemovePatientFalse(){
        testPatientManager = new PatientManager();
        Patient p5 = new Patient("Stevie","Wonder",5555);
        assertFalse(testPatientManager.removePatient(p5.getSocialSecurityNumber()));
    }

    /**
     * Gets patients
     */
    @Test
    void testGetPatients(){
        testPatientManager = new PatientManager();
        Patient p6 = new Patient("Marvin","Gaye",6666);
        testPatientManager.addPatient(p6);
        Patient p7 = new Patient("Lauryn","Hill",7777);
        testPatientManager.addPatient(p7);
        Patient p8 = new Patient("Lenny","Kravitz",8888);
        testPatientManager.addPatient(p8);
        System.out.println(testPatientManager.getPatients());
    }
}
