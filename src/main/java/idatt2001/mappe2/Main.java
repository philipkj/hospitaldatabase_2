package idatt2001.mappe2;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * Main class
 * @version 05.05.2021
 * @author philipkj
 */
public class Main extends Application {

    Stage window;
    TableView table = new TableView();
    PatientManager patientManager = new PatientManager();
    private File currentFile;

    @Override
    public void start(Stage primaryStage){

        window = primaryStage;
        window.setTitle("Patient Register v0.1-SNAPSHOT");

        BorderPane border = new BorderPane();

        //MenuBar
        MenuBar menuBar = new MenuBar();
        border.setTop(menuBar);

        Menu fileMenu = new Menu("File");
        MenuItem fileMenuImport = new MenuItem("Import from .CSV...");
        fileMenu.getItems().add(fileMenuImport);
        fileMenuImport.setOnAction(e -> importFile());
        MenuItem fileMenuExport = new MenuItem("Export to .CSV...");
        fileMenu.getItems().add(fileMenuExport);
        fileMenuExport.setOnAction(e -> exportFile());
        menuBar.getMenus().add(fileMenu);

        Menu editMenu = new Menu("Edit");
        MenuItem editMenuAdd = new MenuItem("Add new patient...");
        editMenu.getItems().add(editMenuAdd);
        editMenuAdd.setOnAction(e -> addNewPatient());
        MenuItem editMenuEdit = new MenuItem("Edit selected patient...");
        editMenu.getItems().add(editMenuEdit);
        editMenuEdit.setOnAction(e -> editSelectedPatient());
        MenuItem editMenuRemove = new MenuItem("Remove selected patient...");
        editMenu.getItems().add(editMenuRemove);
        editMenuRemove.setOnAction(e -> removeSelectedPatient());
        menuBar.getMenus().add(editMenu);

        Menu helpMenu = new Menu("Help");
        MenuItem helpMenuAbout = new MenuItem("About");
        helpMenu.getItems().add(helpMenuAbout);
        menuBar.getMenus().add(helpMenu);
        helpMenuAbout.setOnAction(e -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog - About");
            alert.setHeaderText("Patients register\nv0.1-SNAPSHOT");
            alert.setContentText("A decent application created by\nPhilip Kjus\n04.05.2021");
            alert.showAndWait();
        });

        //Toolbar
        ToolBar toolBar = new ToolBar();
        Button toolBarAddButton = new Button();
        toolBarAddButton.setText("Add"); //TODO change button to images/add_button.png
        toolBar.getItems().add(toolBarAddButton);
        toolBarAddButton.setPrefSize(80,50);
        toolBarAddButton.setOnAction(e -> addNewPatient());
        Button toolBarEditButton = new Button();
        toolBarEditButton.setText("Edit"); //TODO change button to images/edit_button.png
        toolBar.getItems().add(toolBarEditButton);
        toolBarEditButton.setPrefSize(80,50);
        toolBarEditButton.setOnAction(e -> editSelectedPatient());
        Button toolBarRemoveButton = new Button();
        toolBarRemoveButton.setText("Remove"); //TODO change button to images/remove_button.png
        toolBar.getItems().add(toolBarRemoveButton);
        toolBarRemoveButton.setPrefSize(80,50);
        toolBarRemoveButton.setOnAction(e -> removeSelectedPatient());

        //TODO fix images on the buttons. Following code commented out did not work
        //Image testImage = new Image(getClass().getResourceAsStream("images/edit_button.png"));
        //ImageView testImageView = new ImageView(testImage);
        //Button testButton = new Button();
        //button1.setGraphic(testImageView);
        //toolBar.getItems().add(testButton);

        border.setCenter(toolBar);

        //TableColumns
        TableColumn<Patient, String> columnFirstName = new TableColumn<>("First Name");
        columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnFirstName.setPrefWidth(200);
        TableColumn<Patient, String> columnLastName = new TableColumn<>("Last Name");
        columnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnLastName.setPrefWidth(200);
        TableColumn<Patient, String> columnSSN = new TableColumn<>("Social Security Number");
        columnSSN.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        columnSSN.setPrefWidth(200);
        table.getColumns().addAll(columnFirstName,columnLastName,columnSSN);
        border.setBottom(table);

        Scene scene = new Scene(border,800,400);
        window.setScene(scene);
        window.show();
    }

    //TODO create MainController to have less methods in Main

    public void refreshTable(){
        table.getItems().clear();
        table.getItems().addAll(patientManager.getPatients());
    }

    private void addNewPatient(){
        Stage addTaskWindow = new Stage();
        addTaskWindow.initModality(Modality.APPLICATION_MODAL);
        addTaskWindow.setTitle("Add new patient to register");

        GridPane addGrid = new GridPane();
        addGrid.setPadding(new Insets(5,5,5,5));
        addGrid.setHgap(5);
        addGrid.setVgap(5);

        Label firstNameLabel = new Label("First Name:");
        GridPane.setConstraints(firstNameLabel,0,0);
        TextField firstNameInput = new TextField();
        GridPane.setConstraints(firstNameInput,1,0);

        Label lastNameLabel = new Label("Last Name:");
        GridPane.setConstraints(lastNameLabel,0,1);
        TextField lastNameInput = new TextField();
        GridPane.setConstraints(lastNameInput,1,1);

        Label SSNLabel = new Label("Social Security Number:");
        GridPane.setConstraints(SSNLabel,0,2);
        TextField SSNInput = new TextField();
        GridPane.setConstraints(SSNInput,1,2);

        Button addButton = new Button("Add patient");
        GridPane.setConstraints(addButton,0,3);
        addButton.setOnAction(e -> {
            String SSNstring = SSNInput.getText();
            long SSN = Long.parseLong(SSNstring);
            Patient newPatient =  new Patient(firstNameInput.getText(),lastNameInput.getText(),SSN);
            patientManager.addPatient(newPatient);
            addTaskWindow.close();
            refreshTable();
        });
        Button exitButton = new Button("Cancel");
        GridPane.setConstraints(exitButton,1,3);
        exitButton.setOnAction(ee -> addTaskWindow.close());

        addGrid.getChildren().addAll(firstNameInput,firstNameLabel,lastNameInput,lastNameLabel,SSNInput,SSNLabel,exitButton,addButton);
        Scene scene = new Scene(addGrid,350,150);
        addTaskWindow.setScene(scene);
        addTaskWindow.showAndWait();
    }

    private void editSelectedPatient(){
        Patient currentPatient = (Patient) table.getSelectionModel().getSelectedItem();
        Stage editPatientWindow = new Stage();
        editPatientWindow.initModality(Modality.APPLICATION_MODAL);
        editPatientWindow.setTitle("Edit patient in patient register");

        GridPane editGrid = new GridPane();
        editGrid.setPadding(new Insets(5,5,5,5));
        editGrid.setHgap(5);
        editGrid.setVgap(5);

        Label firstNameLabel = new Label("First Name:");
        GridPane.setConstraints(firstNameLabel,0,0);
        TextField firstNameInput = new TextField();
        firstNameInput.setText(currentPatient.getFirstName());
        GridPane.setConstraints(firstNameInput,1,0);

        Label lastNameLabel = new Label("Last Name:");
        GridPane.setConstraints(lastNameLabel,0,1);
        TextField lastNameInput = new TextField();
        lastNameInput.setText(currentPatient.getLastName());
        GridPane.setConstraints(lastNameInput,1,1);

        Label SSNLabel = new Label("Social Security Number:");
        GridPane.setConstraints(SSNLabel,0,2);
        Label SSN = new Label(""+currentPatient.getSocialSecurityNumber());
        GridPane.setConstraints(SSN,1,2);

        Button saveButton = new Button("Save");
        GridPane.setConstraints(saveButton,0,3);
        saveButton.setOnAction(e -> {
            currentPatient.setFirstName(firstNameInput.getText());
            currentPatient.setLastName(lastNameInput.getText());
            editPatientWindow.close();
            refreshTable();
        });
        Button exitButton = new Button("Cancel");
        GridPane.setConstraints(exitButton,1,3);
        exitButton.setOnAction(e -> editPatientWindow.close());

        editGrid.getChildren().addAll(firstNameInput,firstNameLabel,lastNameInput,lastNameLabel,SSN,SSNLabel,exitButton,saveButton);
        Scene scene = new Scene(editGrid,350,150);
        editPatientWindow.setScene(scene);
        editPatientWindow.showAndWait();
    }

    private void removeSelectedPatient(){
        Patient currentPatient = (Patient) table.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText("Delete confirmation");
        alert.setContentText("Are you sure you want to delete this item?");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK){
            patientManager.removePatient(currentPatient.getSocialSecurityNumber());
            refreshTable();
        } else{
            alert.close();
        }
    }

    /**
     * Method importFile opens a file chooser where the user can select .csv files to import from
     */
    public void importFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
        File file = fileChooser.showOpenDialog(null);
        currentFile = file;
        try {
            FileHandler.importFromFile(this.patientManager, file);
            refreshTable();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Import-error.");
            alert.setContentText("Unable to import file, try again.");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        }
    }

    /**
     * Method exportFile opens a file chooser and the user can save .csv file
     */
    public void exportFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
        File file = fileChooser.showSaveDialog(null);
        currentFile = file;
        try {
            FileHandler.exportToFile(this.patientManager, file);
            refreshTable();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Export-error.");
            alert.setContentText("Unable to export file, try again.");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                alert.close();
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
