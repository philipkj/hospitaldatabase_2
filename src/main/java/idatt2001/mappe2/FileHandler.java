package idatt2001.mappe2;

import java.io.*;
import java.nio.file.Files;

/**
 * Class FileHandler handles the reading and writing to file.
 * @version 05.05.2021
 * @author philipkj
 */
public class FileHandler {

    private static final String DIVIDER = ";";

    /**
     * Constructor left empty to avoid creating an instance of FileHandler.
     * The methods are possible to access from other classes
     */
    private FileHandler() { }

    /**
     * Method exportToFile imports a patient list to a file
     * @param patients list of patients
     * @param file file to import from
     * @throws IOException
     */
    public static void importFromFile(PatientManager patients, File file) throws IOException {
        try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath())){
            String line = bufferedReader.readLine();
            while(line != null) {
                Patient patient = getLineAsPatientObject(line);
                if (!patient.getFirstName().equalsIgnoreCase("firstName")) {
                    patients.addPatient(patient);
                }
                line = bufferedReader.readLine();
            }
        }
    }


    /**
     * Method exportToFile exports a patient list to a file
     * @param patients list of patients
     * @param file file to export to
     * @throws IOException
     */
    public static void exportToFile(PatientManager patients, File file) throws IOException {
        PrintWriter printWriter = new PrintWriter(new FileWriter(file));
        for (Patient p : patients.getPatients()) {
            printWriter.println(p.getFirstName() + DIVIDER + p.getLastName() + DIVIDER + p.getSocialSecurityNumber());
        }
        printWriter.close();
    }

    /**
     * Method getLineAsPatientObject gets a line from file to a patient object
     * @param line line to be split into separate values
     * @return patient object from line
     * @throws IOException
     */
    public static Patient getLineAsPatientObject(String line) throws IOException {
        String[] values = line.split(DIVIDER);
        long SSNvalue = Long.parseLong(values[2]);
        Patient patient = new Patient(values[0], values[1],SSNvalue);
        return patient;
    }
}