package idatt2001.mappe2;

/**
 * Class Patient represents a patient at the hospital
 * @version 05.05.2021
 * @author philipkj
 */
public class Patient {

    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;
    private final long socialSecurityNumber; //TODO handle socialSecurityNumber to be 11 numbers long

    /**
     * Constructor with five parameters
     * @param firstName First name of the patient as a String
     * @param lastName Last name of the patient as a String
     * @param diagnosis Diagnosis of the patient as a String
     * @param generalPractitioner Full name of the patient´s general practitioner as a String
     * @param socialSecurityNumber Social security number of the patient as a Long
     */
    public Patient(String firstName, String lastName, String diagnosis, String generalPractitioner, long socialSecurityNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Constructor with three parameters
     * @param firstName First name of the patient as a String
     * @param lastName Last name of the patient as a String
     * @param socialSecurityNumber Social security number of the patient as a Long
     */
    public Patient(String firstName, String lastName, long socialSecurityNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    public long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                ", socialSecurityNumber=" + socialSecurityNumber +
                '}';
    }
}
