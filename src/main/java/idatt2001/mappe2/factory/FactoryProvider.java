package idatt2001.mappe2.factory;

import idatt2001.mappe2.factory.menu.MenuFactory;
import idatt2001.mappe2.factory.menuItem.MenuItemFactory;

public class FactoryProvider {

    public static AbstractFactory<?> getFactory(String choice) {

    if ("Menu".equalsIgnoreCase(choice)) {
        return new MenuFactory();
    } else if ("MenuItem".equalsIgnoreCase(choice)) {
        return new MenuItemFactory();
    }
    return null;
}


}
