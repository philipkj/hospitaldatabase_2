package idatt2001.mappe2.factory.menuItem;

import idatt2001.mappe2.factory.AbstractFactory;

public class MenuItemFactory implements AbstractFactory<MenuItem> {
    @Override
    public MenuItem create(String type) {
        if ("about".equalsIgnoreCase(type)) {
        } else if ("addPatient".equalsIgnoreCase(type)) {
            return new AddMenuItem();
        } else if ("editPatient".equalsIgnoreCase(type)) {
            return new EditMenuItem();
        } return null;
    }

}
