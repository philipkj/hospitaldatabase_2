package idatt2001.mappe2.factory;

public interface AbstractFactory<A> {
    A create(String type);
}
