package idatt2001.mappe2.factory.menu;

import idatt2001.mappe2.factory.AbstractFactory;

public class MenuFactory implements AbstractFactory<Menu> {
    @Override
    public Menu create(String type) {
        if ("editMenu".equalsIgnoreCase(type)) {
            return new EditMenu();
        } else if ("fileMenu".equalsIgnoreCase(type)) {
            return new FileMenu();
        } else if ("helpMenu".equalsIgnoreCase(type)) {
            return new HelpMenu();
        }
        return null;
    }

}
