package idatt2001.mappe2;

import java.util.ArrayList;

/**
 * Class PatientManager manages the adding and removal of patients to the list
 * @version 05.05.2021
 * @author philipkj
 */
public class PatientManager {

    private ArrayList<Patient> patients;

    /**
     * Constructor
     */
    public PatientManager(){
        this.patients = new ArrayList<>();
    }

    public ArrayList<Patient> getPatients(){
        if(patients.size() == 0){
            return null;
        }
        return patients;
    }

    /**
     * Method for adding a patient to the list
     * @param newPatient The patient to be added
     * @return boolean based on wether the patient was successfully added
     */
    public boolean addPatient(Patient newPatient){
        if((patients.size()==0)){
            patients.add(newPatient);
            return true;
        }
        for(Patient p : patients){
            if(!(newPatient.getSocialSecurityNumber() == p.getSocialSecurityNumber())){
                patients.add(newPatient);
                return true;
            }
        }
        return false;
    }

    /**
     * Method for removing a patient from the list
     * @param socialSecurityNumber The social security number of the patient to be removed
     * @return boolean based on wether the removal was successful
     */
    public boolean removePatient(long socialSecurityNumber){
        for(Patient p : patients){
            if(socialSecurityNumber == p.getSocialSecurityNumber()){
                patients.remove(p);
                return true;
            }
        }
        return false;
    }
}
